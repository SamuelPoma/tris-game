$(document).ready(function(){
  // var playerTurn variable that establishes alternate turns of play
  var playerTurn = 0;
  // single cell
  var cell = $(".cell");
  // in this variable I save the classes that define the play signs
  var value;
  // this variable serves to manage a limit cases when a match ends in draw
  var isWinner;
  // event click on cell
  cell.click(function(){
    // singleIdCell takes the value of ID in relation to the clicked cell
    var singleIdCell = $(this).attr("id");
    // if singleIdCell already has the clicked class don't do nothing
    if (!$(this).hasClass("clicked")) {
        // if the player turn is an even number then the game symbol X is inserted
        // else the player turn is an odd number then the game symbol X is inserted
        // X/O classes that will be used to validate the victory / defeat cases with the function playerWin()
        if (playerTurn % 2 == 0) {
              value = "X"
              $(this).html("<i class='fas fa-times'></i>").addClass("sign clicked").addClass(value);
        }
        else{
              value = "O"
              $(this).html("<i class='far fa-circle'></i>").addClass("sign clicked").addClass(value);
        }
        // played counter
        playerTurn++;
        // game sound
        var bipSound = new Audio("audio/bip-game.mp3");
        bipSound.play();
    }
    // initialize every single Id corresponding to a specific cell
    var cell1 =  $("#1");
    var cell2 =  $("#2");
    var cell3 =  $("#3");
    var cell4 =  $("#4");
    var cell5 =  $("#5");
    var cell6 =  $("#6");
    var cell7 =  $("#7");
    var cell8 =  $("#8");
    var cell9 =  $("#9");
    // general statement if for victory/defeat/draw game match
    if(playerWin(value)){
      // statement to define when Player1(X sign) wins.
      if(value=="X") {
        $("#tris-container").addClass("disabled-click").fadeOut(2000);
        setTimeout(function(){
          $("#winner").html("Player 1 Win !").show(1000)
          $("#btn-game").fadeIn(2000)}, 2000);
        var winnerSound = new Audio("audio/taDa.mp3");
        winnerSound.play();
        var isWinner = true;
      }
      // statement to define when Player2(O sign) wins.
      else if(value=="O"){
        $("#tris-container").addClass("disabled-click").fadeOut(2000);
          setTimeout(function(){
            $("#winner").html("Player 2 Win !").show(1000)
            $("#btn-game").fadeIn(2000)}, 2000);
          var winnerSound = new Audio("audio/taDa.mp3");
          winnerSound.play();
          var isWinner = true;
      }
    }
    // statement to define when the match ends in draw
    if (playerTurn == 9 && !isWinner) {
      $("#tris-container").addClass("disabled-click").fadeOut(2000);
      setTimeout(function(){
        $("#winner").html("Draw !").show(1000)
        $("#btn-game").fadeIn(2000)}, 2000);
        var drawGameSound= new Audio("audio/draw.mp3");
        drawGameSound.play();
    }
    // event click: when the match ends a button appears to start new game
    $('#btn-game').click(function() {
      location.reload();
    });
    //the function playerWin() will be used to validate the victory / defeat cases with
    function playerWin(value){
      if ( (cell1.hasClass(value)&& cell2.hasClass(value) && cell3.hasClass(value)) ||
          (cell4.hasClass(value) && cell5.hasClass(value) && cell6.hasClass(value)) ||
          (cell7.hasClass(value) && cell8.hasClass(value) && cell9.hasClass(value)) ||
          (cell1.hasClass(value) && cell4.hasClass(value) && cell7.hasClass(value)) ||
          (cell2.hasClass(value) && cell5.hasClass(value) && cell8.hasClass(value)) ||
          (cell3.hasClass(value) && cell6.hasClass(value) && cell9.hasClass(value)) ||
          (cell1.hasClass(value) && cell5.hasClass(value) && cell9.hasClass(value)) ||
          (cell3.hasClass(value) && cell5.hasClass(value) && cell7.hasClass(value))) {
            return true;
        }
          return false;
      }
  });
});
